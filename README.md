# xml-things
A tool for parse xml file and extract domains and subdomains.


## Installation

```
go install gitlab.com/toolkit3/xml-things@latest
```


## Usage

```
██╗  ██╗███╗   ███╗██╗                         
╚██╗██╔╝████╗ ████║██║                         
 ╚███╔╝ ██╔████╔██║██║                         
 ██╔██╗ ██║╚██╔╝██║██║                         
██╔╝ ██╗██║ ╚═╝ ██║███████╗                    
╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝                    
                                               
████████╗██╗  ██╗██╗███╗   ██╗ ██████╗ ███████╗
╚══██╔══╝██║  ██║██║████╗  ██║██╔════╝ ██╔════╝
   ██║   ███████║██║██╔██╗ ██║██║  ███╗███████╗
   ██║   ██╔══██║██║██║╚██╗██║██║   ██║╚════██║
   ██║   ██║  ██║██║██║ ╚████║╚██████╔╝███████║
   ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝
                     Created by Zero To Hero :)

Usage:
  xml-things [flags]

Flags:
   -file string  xml file to be parsed
   -silent       silent mode

```

###### Created by Sharo_k_h :)
